# import du lieu
from os import pipe
import sys
from requests.api import request 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
import re

# hello1
import requests
import bs4
from bs4 import BeautifulSoup
import json

from sklearn.covariance import ledoit_wolf_shrinkage

# Option
sys.path.insert(0,'/Users/haonguyen/PycharmProjects/selenium_py/chromedriver')

# https://peter.sh/experiments/chromium-command-line-switches/
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless') 
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage') 
chrome_driver = webdriver.Chrome()

# BASE_URL
BASE_URL = 'https://tiki.vn/bestsellers-2020/dien-thoai-may-tinh-bang/c1789'
# Headers 


# Open website
chrome_driver.get(BASE_URL)

#chrome_driver.maximize_window()
sleep(2)

# bs4 for source page
soup = BeautifulSoup(chrome_driver.page_source)

# Get link of items
link_phone_list = []
for link in soup.select('p.title a[href]'):
    link_phone_list.append(link['href'])

#print(link_phone_list)


# Get items id in list products
items_id_list = []
for i in range(len(link_phone_list)):
    txt = link_phone_list[i]
    item_id = re.findall("p\d{8}", txt) 
    item_id = re.sub("p","", item_id[0]) 
    items_id_list.append(item_id)

print(items_id_list)

total_comments = 0

# Get data from api
comment_list = []
def crawling(url):    
    header = {'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.88 Safari/537.36"}
    response = requests.get(url,headers=header)
    data = response.json()['data'] 
    
    for i in range(len(data)):
        if data[i]['content']: # get content vs rating in data           
            comment_list.append({"name":data[i]['created_by']['name'],"comment":data[i]['content'],"rating":data[i]['rating']})
    print("Crawled:", len(comment_list) , "comments.")


# duyệt qua các items
for i in range(len(items_id_list)):
    product_id = items_id_list[i]
    print("Crawling item id:", product_id)
    api_url = f"https://tiki.vn/api/v2/reviews?limit=1000&include=comments,contribute_info&sort=score%7Cdesc,id%7Cdesc,stars%7Call&page=1&product_id={product_id}"
    crawling(api_url)
    total_comments += len(comment_list)


print("Total_comments_crawled: ",total_comments)
# Save to json
#with open('comments_ratings_tiki_crawled.json', 'w',  encoding='utf-8') as file:
    #json.dump(comment_list, file, ensure_ascii=False)

chrome_driver.close()
